package com.marcfarssac.myapplication.util;

import android.content.Context;
import android.util.AttributeSet;

public class TotalClicksTextView extends ResultsTextView {

    public TotalClicksTextView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    @Override
    public void onClick(long time){
        if ((time%2) == 0) winnerClicks++;
        else loserClicks ++;

        setText(String.valueOf(winnerClicks + loserClicks));
    }
}
